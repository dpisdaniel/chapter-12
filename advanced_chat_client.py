__author__ = 'Daniel'
import threading
import socket
EXIT = "EXIT"
SERVER_IP_AND_PORT = '127.0.0.1', 8888
SOCK = socket.socket()
SOCK.connect(SERVER_IP_AND_PORT)


def receive_messages():
    """
    Runs in the background and attempts to receive messages from the server
    """
    while True:
        print SOCK.recv(1024)


if __name__ == "__main__":
    thread = threading.Thread(target=receive_messages)
    thread.start()
    user_name = raw_input("Enter your chat username\n")
    username_len = str(len(user_name))
    while True:
        message = raw_input("Enter the message you would like to send, enter EXIT to leave\n")
        message_len = str(len(message))
        if message == EXIT:
            SOCK.send("")
            break
        message = username_len + user_name + message_len + message
        print message
        SOCK.send(message)
        