__author__ = 'Daniel'
import socket
import select
NAME_RECEIVE_MESSAGE = "Insert a name . . .\n"
SERVER_IP_AND_PORT = '127.0.0.1', 8888


def send_names(sock):
    """
    sock: A socket. The client's socket connected to the server.
    Receives messages from the user and sends them to the server, after that receives a message back from the server
    and prints it.
    """
    while True:
        message = raw_input(NAME_RECEIVE_MESSAGE)
        sock.send(message)
        print sock.recv(1024)


def main():
    sock = socket.socket()
    sock.connect(SERVER_IP_AND_PORT)
    send_names(sock)

if __name__ == "__main__":
    main()
