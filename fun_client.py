__author__ = 'Daniel'
"""
This is a client for the multi client chat server. It receives messages from the user and sends them to the server
(who forwards the messages to all the other clients) and receives messages from the server at the same time.
"""
import threading
import socket
EXIT = "EXIT"
SERVER_IP_AND_PORT = '127.0.0.1', 8888
SOCK = socket.socket()
SOCK.connect(SERVER_IP_AND_PORT)


def receive_messages():
    """
    Runs in the background and attempts to receive messages from the server
    """
    while True:
        print SOCK.recv(1024)


if __name__ == "__main__":
    thread = threading.Thread(target=receive_messages)
    thread.start()
    while True:
        message = raw_input("Enter the message you would like to send, enter EXIT to leave\n")
        if message == EXIT:
            SOCK.send("")
            break
        SOCK.send(message)