__author__ = 'Daniel'
"""
This is a multi client chat server. It receives messages from the clients that connect to the server and forwards them
to all of the other clients that are connected to the server.
"""
import socket
import select
IP_AND_PORT = '127.0.0.1', 8888
END_CONNECTION = ""
CONNECTION_CLOSE_MESSAGE = "Connection with client closed . . .\n"


def create_socket():
    """
    Creates a server socket and returns it.
    """
    server_sock = socket.socket()
    server_sock.bind(IP_AND_PORT)
    return server_sock


def listen_to_clients(server_sock):
    """
    server_sock: The server's socket. SOCKET object.
    listens to multiple clients at the same time.
    """
    server_sock.listen(5)
    handle_clients(server_sock)


def handle_clients(server_sock):
    """
    server_sock: The server's socket. SOCKET object.
    Listens to clients. Handles the multiple clients at the same time, receiving messages from them and forwarding
    messages to them.
    """
    open_client_socks = []
    msgs_to_send = []
    while True:
        rlist, wlist, xlist = select.select([server_sock] + open_client_socks, open_client_socks, [])
        for current_sock in rlist:
            if current_sock is server_sock:
                new_sock, address = server_sock.accept()
                open_client_socks.append(new_sock)
            else:
                data = current_sock.recv(1024)
                if data == END_CONNECTION:
                    open_client_socks.remove(current_sock)
                    print CONNECTION_CLOSE_MESSAGE
                else:
                    msg_already_sent_to = [current_sock]
                    msgs_to_send.append((msg_already_sent_to, data))
        send_waiting_msgs(wlist, msgs_to_send, open_client_socks)


def send_waiting_msgs(wlist, msgs_to_send, open_client_socks):
    """
    wlist: a list of the sockets that data can be sent to them.
    msgs_to_send: a list of tuples, each tuple contains a list of the sockets that the message was already sent to as the
    first element, and the message itself as the second element of the tuple.
    open_client_socks: a list of sockets. A list of all of the clients that are currently connected to the server.
    goes over all of the messages that need to be sent and attempts to send them to all of the clients that can
    currently receive data and haven't already received the message.
    """
    index = 0
    for msg in msgs_to_send:
        msg_already_sent_to, message = msg
        for sock in open_client_socks:
            if sock not in msg_already_sent_to and sock in wlist:
                msg_already_sent_to.append(sock)
                sock.send(message)
        msgs_to_send[index] == msg_already_sent_to, message
        if len(msg_already_sent_to) == len(open_client_socks):
            msgs_to_send.remove(msg)
        index += 1


def main():
    server_sock = create_socket()
    listen_to_clients(server_sock)

if __name__ == "__main__":
    main()
