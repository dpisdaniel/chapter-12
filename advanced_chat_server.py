__author__ = 'Daniel'
import socket
import select
import re
from time import strftime
IP_AND_PORT = '127.0.0.1', 8888
END_CONNECTION = ""
CONNECTION_CLOSE_MESSAGE = "Connection with client closed . . .\n"
REGISTER_USERNAME = 'username='
PARAMETER_DIFFERENTIATOR = '='
USER_LEN_ELEMENT = 0
USERNAME_START = 1
SPACE = " "
FIND_FIRST_DIGITS = '(\d+)'


class AdvancedChatServer():
    """

    """
    def __init__(self, ip_and_port):
        """
        ip_and_port: A tuple. An IP string as the first element and port number as the second element.
        creates the server socket and saves it as a class parameter.
        """
        self.server_sock = socket.socket()
        self.server_sock.bind(ip_and_port)

    def listen_to_clients(self):
        """
        Enables clients to connect to the server.
        """
        self.server_sock.listen(5)

    def handle_clients(self):
        """
        Listens to clients. Handles the multiple clients at the same time, receiving messages from them and forwarding
        messages to them.
        """
        open_client_socks = []
        msgs_to_send = []
        while True:
            rlist, wlist, xlist = select.select([self.server_sock] + open_client_socks, open_client_socks, [])
            msgs_to_send, open_client_socks = self._iterate_sockets(rlist, open_client_socks, msgs_to_send)
            self._send_waiting_msgs(wlist, msgs_to_send, open_client_socks)

    def _iterate_sockets(self, rlist, open_client_socks, msgs_to_send):
        """
        rlist: a list of sockets. Contains all the sockets that data can be currently read from.
        open_client_socks: a list of sockets. Contains all the sockets that are currently connected to the server.
        msgs_to_send: a list of tuples, each tuple contains a list of the sockets that the message was already sent to
        as the first element, and the message itself as the second element of the tuple.

        Iterates over the sockets in the rlist, receives the messages from them or creating connections with new
        clients, and arranging all of the messages that need to be sent to the rest of the clients.
        Returns the updated msgs_to_send and open_client_socks
        """
        for current_sock in rlist:
            if current_sock is self.server_sock:
                new_sock, address = self.server_sock.accept()
                open_client_socks.append(new_sock)
            else:
                data = current_sock.recv(1024)
                if data == END_CONNECTION:
                    open_client_socks.remove(current_sock)
                    print CONNECTION_CLOSE_MESSAGE
                else:
                    message_str = self._create_message_string(data)
                    msg_already_sent_to = [current_sock]
                    msgs_to_send.append((msg_already_sent_to, message_str))
        return msgs_to_send, open_client_socks

    def _create_message_string(self, data):
        """
        data: a string. Contains all the information for the server as to how to build the message string that will
        be sent to all the clients in the end.

        Builds the message that will be sent to the clients.
        #NEEDS WORK HERE!!!!
        """
        time = strftime("%H:%M")
        username_len = re.search(FIND_FIRST_DIGITS, data)
        length = username_len.group(1)
        username = data[USERNAME_START:int(length) + 1]
        message_str = time + SPACE + username + ": " + data[int(length) + 1:]
        return message_str

    def _send_waiting_msgs(self, wlist, msgs_to_send, open_client_socks):
        """
        wlist: a list of the sockets that data can be sent to them.
        msgs_to_send: a list of tuples, each tuple contains a list of the sockets that the message was already sent to as the
        first element, and the message itself as the second element of the tuple.
        open_client_socks: a list of sockets. A list of all of the clients that are currently connected to the server.

        goes over all of the messages that need to be sent and attempts to send them to all of the clients that can
        currently receive data and haven't already received the message.
        """
        index = 0
        for msg in msgs_to_send:
            msg_already_sent_to, message = msg
            for sock in open_client_socks:
                if sock not in msg_already_sent_to and sock in wlist:
                    msg_already_sent_to.append(sock)
                    sock.send(message)
            msgs_to_send[index] == msg_already_sent_to, message
            if len(msg_already_sent_to) == len(open_client_socks):
                msgs_to_send.remove(msg)
            index += 1


def main():
    """
    Sets up a simple AdvancedChatServer object and creates a chat server with it.
    """
    chat = AdvancedChatServer(IP_AND_PORT)
    chat.listen_to_clients()
    chat.handle_clients()

if __name__ == "__main__":
    main()
