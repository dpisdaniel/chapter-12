__author__ = 'Daniel'
import socket
import select
IP_AND_PORT = '127.0.0.1', 8888
END_CONNECTION = ""
CONNECTION_CLOSE_MESSAGE = "Connection with client closed . . .\n"


def create_socket():
    """
    Creates a server socket and returns it.
    """
    server_sock = socket.socket()
    server_sock.bind((IP_AND_PORT))
    return server_sock


def listen_to_clients(server_sock):
    """
    server_sock: The server's socket. SOCKET object.
    listens to multiple clients at the same time.
    """
    server_sock.listen(5)
    handle_clients(server_sock)


def handle_clients(server_sock):
    """
    server_sock: The server's socket. SOCKET object.
    Listens to clients. Handles the multiple clients at the same time, receiving messages from them and forwarding
    messages back to the same clients who sent the messages.
    """
    open_client_socks = []
    msgs_to_send = []
    while True:
        rlist, wlist, xlist = select.select([server_sock] + open_client_socks, open_client_socks, [])
        for current_sock in rlist:
            if current_sock is server_sock:
                new_sock, address = server_sock.accept()
                open_client_socks.append(new_sock)
            else:
                data = current_sock.recv(1024)
                if data == END_CONNECTION:
                    open_client_socks.remove(current_sock)
                    print CONNECTION_CLOSE_MESSAGE
                else:
                    msgs_to_send.append((current_sock, 'Hello, ' + data))
        send_waiting_msgs(wlist, msgs_to_send)


def send_waiting_msgs(wlist, msgs_to_send):
    """
    wlist: a list of the sockets that data can be sent to them.
    msgs_to_send: a list of tuples. Each tuple contains the socket that the message was sent from as the first element
    and the message that it sent as the second element.
    Attempts to send all of the messages that need to be sent back to the clients who sent them.
    """
    for msg in msgs_to_send:
        client_sock, data = msg
        if client_sock in wlist:
            client_sock.send(data)
            msgs_to_send.remove(msg)


def main():
    server_sock = create_socket()
    listen_to_clients(server_sock)

if __name__ == "__main__":
    main()
